import numpy as np
import cv2


def resize(img, w, h):
    return cv2.resize(img, (w, h))


class Modeling:
    def __init__(self, etl_obj):
        """Creating the model constructor"""
        #  add etl_obj as one of the properties of modeling class
        self.etl = etl_obj

    def centroid_tracking(self):
        ret = True
        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video = cv2.VideoWriter('assets/temp.avi',
                                fourcc,
                                self.etl.fps, (self.etl.config[self.etl.file_type]['resize_w'],
                                               self.etl.config[self.etl.file_type]['resize_h']))
        while ret:
            # Retrieve read frames from the etl object
            if self.etl.file_type == 'Webcam':
                frames = self.etl.cap.read()
            else:
                ret, frames = self.etl.cap.read()
            if ret is False:
                break

            # convert frame in RGB to HSV (hue, saturation, color) frame
            hsv = cv2.cvtColor(frames, cv2.COLOR_BGR2HSV)

            # create a mask for identifying red and green objects
            red_mask = cv2.inRange(hsv, np.array(self.etl.config['masks']['red']['l_b']),
                                   np.array(self.etl.config['masks']['red']['u_b']))
            green_mask = cv2.inRange(hsv, np.array(self.etl.config['masks']['green']['l_b']),
                                     np.array(self.etl.config['masks']['green']['u_b']))
            masks = [red_mask, green_mask]

            # find contours and draw rectangle in the original video for each mask
            for objectID, mask in enumerate(masks):
                contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                if not contours:
                    print('[INFO] Desired range of color not detected!')
                    continue
                max_contour = contours[0]
                for contour in contours:
                    if cv2.contourArea(contour) > self.etl.config[self.etl.file_type]['max_contour_thresh']*cv2.contourArea(max_contour):
                        max_contour = contour

                contour = max_contour
                approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
                x, y, w, h = cv2.boundingRect(approx)
                cv2.rectangle(frames, (x, y), (x + w, y + h), (0, 255, 0), 4)

            # Find centroid and mark using a small circle
                m1 = cv2.moments(contour)
                if m1['m00'] != 0:
                    cx = int(m1['m10'] // m1['m00'])
                    cy = int(m1['m01'] // m1['m00'])
                else:
                    cx = int(m1['m10'] // 0.1)
                    cy = int(m1['m01'] // 0.1)
                cv2.circle(frames, (cx, cy), 3, (255, 0, 0), -1)
                text = "ID {}".format(objectID)
                cv2.putText(frames, text, (cx - 10, cy - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
                resized = resize(frames, self.etl.config[self.etl.file_type]['resize_w'],
                                       self.etl.config[self.etl.file_type]['resize_h'])
            cv2.imshow("frame", resized)
            video.write(resized)
            k = key = cv2.waitKey(self.etl.config[self.etl.file_type]['show_lag'])
            if key == ord('q') or k >= 60:
                break
        cv2.waitKey(0)
        cv2.destroyAllWindows()
