from etl import ETL
from modeling import Modeling


def main(file_information):
    etl_obj = ETL()  # initialize an ETL object
    success = etl_obj.preprocess(file_information)  # preprocess the loaded file
    if success is False:
        print('[INFO] Aborted the process.')
        return
    model = Modeling(etl_obj)  # initialize the modeling object
    model.centroid_tracking()  # centroid track the objects


# Call Main function
# file_name = "ballmotion.m4v" # simple ball motion
# Below: different files available for testing the code
file_name = "Bouncing_Red_ball.m4v"  # fast and complex ball motion
file_name = 'phone_eg_video.mp4'  # video made on phone
file_name = 'multiple_balls.mp4' # tracking multiple objects
# file_type = 'Webcam'  # other options - 'Video'
file_type = 'Video'
config_file = 'Config.yaml'  # name of the config file
file_info = (file_name, file_type, config_file)  # create a tuple of file information
main(file_info)  # call main()
