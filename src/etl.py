import os
import imageio
from cv2 import cv2
import yaml
from imutils.video import VideoStream
import time


class ETL:
    def __init__(self):
        """Creating the ETL constructor"""
        # initialize the ETL constructor
        self.config = {'Image': {'relative_path': 'assets',
                                 'mp_r': 104.0, 'mp_g': 177.0,
                                 'mp_b': 123.0}}  # Initialization
        self.data_dir = []  # directory where main file is kept
        self.src_dir = os.path.dirname(__file__)  # src dir containing code files
        self.root_dir = os.path.dirname(self.src_dir)  # Root directory containing src and data dir
        self.file_title = 'ballmotion.m4v'  # initialize with eg data
        self.file_type = 'Video'  # Initialize with other option 'Webcam'
        self.config_file_name = 'Config.yaml'  # Initialize the config file name
        self.cap = []  # read object for the video file/ webcam video
        self.fps = []  # fps of the loaded video

    def load_from_src(self):
        """Loading files from the source"""
        with open(os.path.join(self.root_dir, self.config_file_name), "r") as yamlfile:
            self.config = yaml.safe_load(yamlfile)
        frames, success = [], True
        if self.file_type == 'Image' or self.file_type == 'Video':
            # if source is image/video, load from the data dir
            self.data_dir = os.path.join(self.root_dir, self.config[self.file_type]['relative_path'])
            dir_files = os.listdir(self.data_dir)
            if self.file_title in dir_files:
                file_path = os.path.join(self.data_dir, self.file_title)
                if self.file_type == 'Image':
                    frames = imageio.mimread(file_path)
                elif self.file_type == 'Video':
                    frames = cv2.VideoCapture(file_path)
                    self.fps = frames.get(cv2.CAP_PROP_FPS)

            else:
                print('[ERROR] File not found on the path')
        elif self.file_type == 'Webcam':
            # if source is a webcam, then start a videostream
            print("[INFO] Starting video stream...")
            frames = VideoStream(src=0).start()
            time.sleep(2.0)
        else:
            print('[ERROR] Unknown file type provided!')
            success = False
        return frames, success

    def preprocess(self, file_info):
        # extract data file title, type and config file name from the argument
        self.file_type = file_info[1]
        if self.file_type == 'Webcam':
            self.file_title = 'stream_video'
        else:
            self.file_title = file_info[0]
        self.config_file_name = file_info[2]
        success = False
        # load data file from source
        self.cap, success = self.load_from_src()
        if success is False:
            print('[ERROR] Failed to load from source')
        return success


