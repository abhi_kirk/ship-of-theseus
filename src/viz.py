# visit http://127.0.0.1:8050/ in your web browser.
import dash
from dash import dcc
from dash import html
import dash_player as player
import dash_bootstrap_components as dbc

import pandas as pd
import numpy as np

from dash.dependencies import Input, Output
from plotly import graph_objs as go
from plotly.graph_objs import *

external_stylesheets = [dbc.themes.BOOTSTRAP]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.title = "ASL to Text Converter"
server = app.server

# Dictionary of example tracking videos
list_of_files = {
    # "Webcam": {"name": "Webcam", "type": "Webcam"},
    "Single Ball Lateral Motion": {"name": "ballmotion.m4v", "type": "Video"},
    "Single Bouncing Ball": {"name": "Bouncing_Red_ball.m4v", "type": "Video"},
    "Multiple Bouncing Balls": {"name": "multiple_balls.mp4", "type": "Video"},
    "Phone Video 1": {"name": "phone_eg_video.mp4", "type": "Video"},
}

# Layout of Dash App
app.layout = html.Div(
    [
        dbc.Row(
            [
                dbc.Col(
                    html.Div(
                        className="four columns div-user-controls",
                        children=[
                            html.A(
                                html.Img(
                                    className="logo",
                                    src=app.get_asset_url("ASL_Logo.png"),
                                    style={'height': '50%', 'width': '100%'}
                                ),
                            ),
                            html.H2("American Sign Language to Text Converter",
                                    ),
                            html.P(
                                """Select an example video below to begin"""
                            ),
                            html.Div(
                                className="div-for-dropdown",
                                children=[
                                    # Dropdown for example videos
                                    dcc.Dropdown(
                                        id="video-dropdown",
                                        options=[
                                            {"label": i, "value": i}
                                            for i in list_of_files
                                        ],
                                        placeholder="Select an eg video",
                                    )
                                ],
                            ),
                            html.Div(
                                dcc.Markdown(
                                    """
                                    Choose 'webcam' option to open the camera and track hand gestures for ASL to text conversion.  
                                    Examples to try:  
                                     - Single ball lateral motion
                                     - Single bouncing ball
                                     - Multiple bouncing balls
                                     - Example phone video
                                    
                                                                  """
                                ), style={'marginTop': 20},
                            ),
                        ],
                    ),
                    width={"size": 4},
                ),
                dbc.Col(
                    html.Div(
                        className="eight columns div-for-charts bg-grey",
                        children=[
                            dbc.Row(
                                html.Div(
                                    className='video-outer-container',
                                    children=html.Div(
                                        style={'width': '100%', 'paddingBottom': '45%', 'position': 'relative'},
                                        children=player.DashPlayer(
                                            id='video-display',
                                            style={'position': 'absolute', 'width': '100%',
                                                   'height': '100%', 'top': '0', 'left': '0', 'bottom': '0', 'right': '0'},
                                            # url='https://www.youtube.com/watch?v=wa0nxppMJ-Q',
                                            # url=app.get_asset_url('ballmotion.m4v'),
                                            controls=True,
                                            playing=False,
                                            volume=1,
                                        )
                                    ),
                                ),
                            ),
                            dbc.Row(
                                html.Div(
                                    [
                                        dcc.Textarea(
                                            id='text_area',
                                            value='<Converted Text>',
                                            style={'width': '100%', 'height': '300%'},
                                        ),
                                    ],
                                )
                            ),
                        ],
                    ),
                    width={"size": 8},
                ),
            ]
        ),
    ]
)


# Update textarea based on dropdown option
@app.callback(
    Output("text_area", "value"),
    Input("video-dropdown", "value")
)
def update_text(video_picked):
    if video_picked is None:
        file_name = None
    else:
        file_name = list_of_files[video_picked]["name"]
    return 'You have selected "{}"'.format(file_name)


# Update dash player based on dropdown option
@app.callback(
    Output("video-display", "url"),
    Output("video-display", "style"),
    Input("video-dropdown", "value")
)
def update_video(video_picked):
    url = 'https://www.youtube.com/watch?v=wa0nxppMJ-Q'
    style = {'position': 'absolute', 'width': '100%',
             'height': '100%', 'top': '0', 'left': '0', 'bottom': '0', 'right': '0'}
    if video_picked is None:
        url = 'https://www.youtube.com/watch?v=wa0nxppMJ-Q'
    else:
        file_name = list_of_files[video_picked]["name"]
        url = app.get_asset_url(file_name)
    return url, style


if __name__ == '__main__':
    app.run_server(debug=True)
