# Ship of Theseus

## Summary
Sign language translation to text and vice versa with image processing tools

## Objectives
- Get upto speed with latest Python data science tools
- Utilize OO framework
- Agile approach with Git, CI/CD, Jira
- Image processing for ASL recognition with point-tracking 
- Studying and implementing various ML/DL algorithms 
- JS React for dashboard
- Open-source for community support

## Specifications
- python v3.8.8
- requirements.txt for venv